# Working in a development environment on your laptop

### System software and configuration requirements

<details>
<summary>Go</summary>

- From your terminal, check to see if the Go programming language is already installed:
  ```bash
  go version
  ```
  > You should see some version installed such as: `go version go1.20.3 darwin/arm64`

- If you see don't see a version of Go installed on your computer follow this [video](/media/Go_Install.mp4).
- [Documentation](https://go.dev/doc/)
</details>


<details>
<summary>Rancher Desktop</summary>

- Rancher Desktop will serve as the Docker runtime environment for your local containers
- Follow the installation instructions [here](https://docs.rancherdesktop.io/getting-started/installation/#installing-rancher-desktop-on-macos).
- [Documentation](https://docs.rancherdesktop.io/)
</details>


<details>
<summary>Homebrew</summary>

- Install with the following command from the terminal:
  ```bash
  /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
  ```
- [Documentation](https://brew.sh/)
</details>


<details>
<summary>mkcert</summary>

- Install with the following command from the terminal:
  ```bash
  brew install mkcert
  ```
- [Documentation](https://www.npmjs.com/package/mkcert)
</details>


<details>
<summary>Git</summary>

- Install with the following command from the terminal:
  ```bash
  brew install git
  ```
- [Documentation](https://git-scm.com/)
</details>


<details>
<summary>Yarn</summary>

- Install with the following command from the terminal:
  ```bash
  brew install yarn
  ```
- [Documentation](https://yarnpkg.com/)
</details>


<details>
<summary>Edit localhost DNS entry</summary>

- Open a terminal on your laptop
- Edit your `/etc/hosts` file using the `nano` edior with `sudo` permissions:
  ```bash
  sudo nano /etc/hosts
  ```
- Change the line with `"127.0.0.1"`, to reflect the code below:
  ```bash
  127.0.0.1 localhost keycloak.gap.dev mhub.gap.dev
  ```
- Save by pressing <kbd>ctrl</kbd> + <kbd>o</kbd>
- Press <kbd>enter</kbd> when prompted to save
- Exit by pressing <kbd>ctrl</kbd> + <kbd>x</kbd>
</details>

# Building/Running Application locally on a Mac

- Clone the `dominion` code repository to your local computer:
  ```
  git clone https://gitlab.jadeuc.com/gap/applications/operations/dominion.git
  ```
- `Shut down the Rancher Desktop` application and any other docker runtime that you have running since they can interfere with the creation of SSL certificates
- In the newly cloned local repository, switch to the `/dominion/mhub` directory
- From the `/dominion/mhub` directory create SSL certificates with the following command:
  ```bash
  make ssl
  ```
  > This will create a local cert so mhub will look like a real website. If you skip this step you will have authentication errors in your browser.
- From the `/dominion/mhub` directory build the application containers with the following command:
  ```bash
  make -C ../containers
  ```
- Start your front end client as a background process:
  ```bash
  cd client && yarn install && yarn start &
  ```
- Create the `"docker-compose.override.yml"` file in the `/dominion/mhub` directory with the following contents:
  ```yaml
  services:
    db:
      ports:
        - 3306:3306
  
    minio:
      ports:
        - 9000:9000
  
  ```
  > Spacing is crutial in .yml files.  Ensure that you create the file exactly as shown above.
- Start the local development server with the following comand:
  ```bash
  make local-dev
  ```
- All of the containers should start:

  ![image](./media/App_Running.jpeg)

- Once all of the containers have started, use Google Chrome to navigate to the [Mission Hub](https://mhub.gap.dev) local application site.

  ![image]./media/App_Login.png)

- Shutdown the application from the terminal with <kbd>ctrl</kbd> + <kbd>c</kbd>



<details>
<summary><i>Archived content</i></summary>

- From repository root run ```docker compose up -d```
- Once the containers spin up, navigate to mhub.gap.dev and sign in as admin/password to see the application!
- For those sweet sweet logs ``` docker compose logs mhub -f```


#### Container Run Server

-```make container-dev```
- docker-compose.override.yml can stay as is

### Building/Running Application locally wiht Windows with WSL2
- run this command to install brew ```/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"```
- install mkcert if not installed already ```brew install mkcert```

- Docker cannot be running when you make the cert for reasons.

- From the root directory run ```make ssl```. This will create a local cert so mhub will look like a real website.
- Open notepad via windows start menu then open ```C:\Windows\System32\Drivers\etc\hosts``` file. NOTE: You will need to change the file type in bottom left corner to see this file.
- Edit the hosts file by adding ```keycloak.gap.dev``` and ```mhub.gap.dev``` to the localhost line (127.0.0.1 line). Save the file and close. NOTE: You will have to remove the # sign from these lines if its there.
- After this check your WSL /etc/hosts file for continuity, edit that file if neccessary.
- Opwn your windows Powershell terminal. Install scoop or any other package manager. These commands will install Scoop ```Set-ExecutionPolicy RemoteSigned -Scope CurrentUser``` ```irm get.scoop.sh | iex```
- Then install git ```scoop install git``` then add extras and install mkcert ```scoop bucket add extras```>```scoop install mkcert```>```mkcert -install``` 
- Now CD into your Desktop for easy access andrunn the command ```mkcert -pkcs12 localhost``` this will create your cert on the desktop in order for ease of access.
- Type ```mkcert -CAROOT``` into the command line of both your WSL terminal and windows powershell.
- Navigate to the directory and copy the Windows cert into the WSL cert location.
- Finished creating your certs.

- Next go into your code environment. In the terminal ```type ip addr``` in order to get your WSL2's IP address.
- Once you have this address, navigate to the `/dev/"nginx/templates"/default.conf.template`. In the location / template go to the proxy pass line and insert youripaddress:3000. EXAMPLE: http://172.20.247.160:3000
- Save all files. Then start Docker Desktop
- From the repository root, run ```make -C containers``` this will build the application container to be deployed to docker shortly
- start your front end client ```cd client && yarn install && yarn start```
- From repository root run ```docker compose up -d```
- Once the containers spin up, navigate to mhub.gap.dev and sign in as admin/password to see the application!
- For those sweet sweet logs ``` docker compose logs mhub -f```




### Using the Makefile

Requires Go to be setup to run make commands

#### Run makefile scripts with `make` command: `make build`

#### Available scripts:
``` 
all                            Runs 'make build-dev', 'make server-dep' and 'up' to spin up docker containers
client-dep                     Install frontend dependencies
build                          Build the client app into build/ directory
build-dev                      Build the client app in development mode
start                          Start the client app as a dev-server
lint                           Run linter on frontend
lint-fix                       Run the linter with the --fix flag
server-dep                     Get Go dependencies
run                            (Deprecated: Server now runs in docker file) Run Go server from server/main.go file
go-lint                        Run linter on the backend
binary                         Build the binary for mhub and put it in /build
client-test                    Run the frontend unit tests
server-test                    Run the backend unit tests
journey                        Run journey test
journey-pipeline               Run journey tests for pipeline
journey-headed                 Run journey tests with browser
e2e-open-tests                 Open cypress tests in cypress window
test                           Run all tests for the project
nats-sub                       View Logs for mhub_nats_box container 
logs                           Show backend logs
scan                           Runs Sonarqube and shows logs for scanner
clean                          Remove previous build
up                             Start Docker Containers
down                           Stop Docker Containers
help                           Display this help screen
```

#### When editing the make file you might see this error:
`Makefile:1: *** missing separator.  Stop.`

That means that an indention on one of your lines is the wrong kind of TAB character
https://stackoverflow.com/questions/920413/make-error-missing-separator

</details>